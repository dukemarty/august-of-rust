fn main() {
    const MAX_POINTS: u32 = 100_000;
    println!("That's a constants: MAX_POINTS = {}", MAX_POINTS);

    let mut x = 5;
    println!("The value of x is: {}", x);
    x = 6;
    println!("The value of x is: {}", x);
}
