
fn float_types() {
    println!("float types\n-----------\n");

    let _x = 2.0; // f64
    println!("Type of x (2.0, implicit): f64");

    let _y: f32 = 3.0; // f32
    println!("Type of y (3.0): f32 explicit");

    println!("\n\n");
}

fn numeric_operations(){
    println!("numeric operations\n------------------\n");

    // addition
    let sum = 5 + 10;
    println!("5 + 10 = {}", sum);

    // subtraction
    let difference = 95.5 - 4.3;
    println!("95.5 - 4.3 = {}", difference);

    // multiplication
    let product = 4 * 30;
    println!("4 * 30 = {}", product);

    // division
    let quotient = 56.7 / 32.2;
    println!("56.7 / 32.2 = {}", quotient);

    // remainder
    let remainder = 43 % 5;
    println!("43 % 5 = {}", remainder);

    println!("\n\n");
}

fn tuples(){
    println!("tuples\n------\n");

    let tup: (i32, f64, u8) = (500, 6.4, 1);

    let (_x, y, _z) = tup;

    println!("tup = ({}, {}, {})", tup.0, tup.1, tup.2);
    println!("y = {}", y);

    println!("\n\n");
}

fn arrays(){
    println!("array\n-----\n");

    let a = [1, 2, 3, 4, 5];
    println!("a[0]: {}", a[0]);

    let months = ["January", "February", "March", "April", "May", "June", "July",
                  "August", "September", "October", "November", "December"];
    println!("#months: {}", months.len());

    let _b: [i32; 5] = [6, 7, 8, 9, 10];

    println!("\n\n");
}

fn main() {
    float_types();

    numeric_operations();

    tuples();

    arrays();
}
