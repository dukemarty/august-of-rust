fn main() {
    let x = 5;
    println!("1st x: {}", x);

    let x = x + 1;
    println!("2nd x: {}", x);

    let x = x * 2;
    println!("3rd x: {}", x);
}
